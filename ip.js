const ipBtn = document.getElementById("ip-btn")
const ip = document.getElementById("ip")
const ipCountry = document.getElementById("ip-country")
const ipCity = document.getElementById("ip-city")
const ipISP = document.getElementById("ip-isp")
const ipWeather = document.getElementById("ip-weather")
const map = document.getElementById("map")
ipBtn.addEventListener("click", () => {

    const getWeatherInfo = (lat, long) => {
        fetch(`https://api.met.no/weatherapi/locationforecast/2.0/compact?lat=${lat}&lon=${long}`)
            .then(res => res.json())
            .then(data => {
                console.log("data", data)
                ipWeather.innerText = data.properties.timeseries[0].data.instant.details.air_temperature + " °C"
            })
    }

    const setWeatherMap = (lat, long) => {
        const zoom = 0.2
        const url = `https://www.openstreetmap.org/export/embed.html?bbox=${long - zoom},${lat - zoom},${long + zoom},${lat + zoom}&marker=${lat},${long}`
        map.hidden = false
        map.src = url
    }

    const getIpInfo = ip => fetch(`http://ip-api.com/json/${ip}`)
        .then(res => res.json())
        .then(data => {
            ipCountry.innerText = data.country
            ipCity.innerText = data.city
            ipISP.innerText = data.isp
            const lat = data.lat
            const long = data.lon
            setWeatherMap(lat, long)
            getWeatherInfo(lat, long)

        })

    fetch("https://api.ipify.org/?format=json")
        .then(res => res.json())
        .then(data => {
            ip.innerText = data.ip
            getIpInfo(data.ip)
        })
})